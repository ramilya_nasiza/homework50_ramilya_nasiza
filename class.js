//механизм//

class Machine {
    constructor(){
    this.turn = false;

    this.turnOn = () => this.turn =true;

    this.turnOff = () => this.turn = false;
  }
}

//бытовой прибор//

class HomeAppliance extends Machine{
  constructor(){
    super();
    this.plug = false;

    this.plugIn = () => {
      this.plug = true;
      console.log('The plug is in');
    };

    this.plugOff = () => {
      this.plug = false;
      console.log('The plug is off');
    };
  }
}

//стиральная машина//

class WashingMachine extends HomeAppliance{
  constructor(){
    super();
    this.turnOn = () => {
      if (this.plug) {
        this.turn = true;
        console.log("Machine is on");
      } else console.log("Check if the plug on!");
    };
    this.run = () => {
      if (this.plug && this.turn) console.log('Is starting!');
      else console.log("You should check plug or turn");
    };
  }
}

//источник света//

class LightSource extends HomeAppliance{
  constructor(){
    super();
    this.level = 0;

    this.turnOn = () =>{
      if (this.plug) {
        this.turn = true;
        console.log("Light is on");
      } else console.log("Check if the plug on!");
    };

    this.setLevel = (level) => {
      if(this.turn) {
        if (level >= 0 && level <= 100) {
          this.level = level;
          console.log('The level is ' + level);
        } else {
          console.log("Incorrect level");
        }
      } else console.log('chek is on');
    };
  }
}

//автоматическое транспортное средство//

class AutoVehicle extends Machine{
  constructor(){
    super();
    this.x = 0;
    this.y = 0;

    this.setPosition = (x, y) =>{
      this.x = x;
      this.y = y;
    };
  }
}

//автомобиль//

class Car extends AutoVehicle{
  constructor(){
    super();
    this.speed = 10;

    this.setSpeed = (speed) =>{
      if(this.turn){
        this.speed = speed;
        console.log('The speed is: ' + this.speed);
      } else console.log('check turn and position');
    };

    this.run = function(x, y) {
      if(this.turn){
        const interval = setInterval(() => {
          let newX = this.x + this.speed;
          if(newX > x) newX = x;
          let newY = this.y + this.speed;
          if(newY > y) newY = y;
          this.setPosition(newX, newY);
          console.log('Car is moving from ' + newX + ' to ' + newY);
          if(newX >= x && newY >= y){
            clearInterval(interval);
            console.log('The car is arrived');
          }
        }, 1000);
      } else console.log('Cant move, because the vehicle is off');

    };
  }
}